"""Файл конфигурации приложения Django."""

from django.apps import AppConfig


class AuthApiConfig(AppConfig):
    """Класс конфигурации приложения AuthAPI в Django."""

    name = 'auth_api'
