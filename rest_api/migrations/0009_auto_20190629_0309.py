# Generated by Django 2.2.2 on 2019-06-29 00:09

from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('rest_api', '0008_auto_20190629_0305'),
    ]

    operations = [
        migrations.AlterField(
            model_name='avatar',
            name='file',
            field=models.ImageField(upload_to='avatars', verbose_name='Файл'),
        ),
    ]
